$.validator.addClassRules({
    sku: {
        required: true,
        minlength: 1
    },
    name: {
        required: true,
        minlength: 1
    },
    price: {
        required: true,
        minlength: 1
    },
    type: {
        required: true,
        minlength: 1
    },
    size: {
        required: true,
        minlength: 1
    },
    weight: {
        required: true,
        minlength: 1
    },
    dimensions_h: {
        required: true,
        minlength: 1
    },
    dimensions_w: {
        required: true,
        minlength: 1
    },
    dimensions_l: {
        required: true,
        minlength: 1
    }
});

$(".validate").validate({
    rules: {
        sku: "required",
        name: "required",
        price: "required",
        type: "required",
        size: "required",
        weight: "required",
        dimensions_h: "required",
        dimensions_l: "required",
        dimensions_w: "required"
    },
    messages: {
        sku: "Please, provide sku",
        name: "Please, provide name",
        price: "Please, provide price",
        type: "Please, provide the data of indicated type",
        size: "Please, provide size",
        weight: "Please, provide weight",
        dimensions_h: "Please, provide dimensions height",
        dimensions_l: "Please, provide dimensions lenght",
        dimensions_w: "Please, provide dimensions weight"
    }
});