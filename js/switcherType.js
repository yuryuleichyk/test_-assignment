$(document).ready(function(){
        $('#input_type').change(function(i){
        if (this.value == 'dvd')
        {
            $('#dvd_specs').removeClass('no_display');
            $('#books_specs').addClass('no_display');
            $('#furniture_specs').addClass('no_display');
        }
        else if (this.value == 'book')
        {
            $('#dvd_specs').addClass('no_display');
            $('#books_specs').removeClass('no_display');
            $('#furniture_specs').addClass('no_display');
        }
        else if (this.value == 'furniture')
        {
            $('#books_specs').addClass('no_display');
            $('#dvd_specs').addClass('no_display');
            $('#furniture_specs').removeClass('no_display');
        }
        else
        {
            $('#books_specs').addClass('no_display');
            $('#dvd_specs').addClass('no_display');
            $('#furniture_specs').addClass('no_display');
        }
    });

});