<?php


require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'models/ProductRepository.php';
require_once 'models/Products/BookProduct.php';
require_once 'models/Products/DvdProduct.php';
require_once 'models/Products/FurnitureProduct.php';
require_once 'core/route.php';

Route::start();
