<?php

declare(strict_types=1);

class ProductNotFoundExceptions extends \Exception
{
    public function __construct($message = "Product not found.", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}