<?php

interface ProductInterface
{
    public function getName(): string;

    public function getPrice(): float;

    public function getSKU(): string;

    public function save($pdoConnector);

    public function delete($pdoConnector);
}