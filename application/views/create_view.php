

<form method="post"  action="/add" class="validate">
    <div class="row">
        <div class="col-3"><h2>Products Add</h2></div>
        <div class="col-3 ml-auto">
            <a href="/" class="btn btn-secondary active btn-sm float-md-right mt-2 mr-2" role="button" aria-pressed="true">Cancel</a>
            <button type="submit" class="btn btn-success float-md-right mt-2 mr-2 btn-sm btn_submit">Save</button>
        </div>
    </div>
    <hr/>
    <div class="form-group vid">
        <label for="input_sku">SKU</label>
        <input type="text" class="required form-control" id="input_sku" name="sku" />
    </div>
    <div class="form-group vid">
        <label for="input_name">Name</label>
        <input type="text" class="required form-control"  id="input_name" name="name" />
    </div>

    <div class="form-group vid">
        <label for="input_price">Price</label>
        <input type="number" class="required form-control" step="0.01" min="0.01"  id="input_price" name="price" />
    </div>

    <div class="form-group">
        <label for="input_type">Type</label>
        <select  class="required form-control" id="input_type" name="type">
            <option value="">Select type</option>
            <option value="dvd">DVD Disk</option>
            <option value="book">Book</option>
            <option value="furniture">Furniture</option>
        </select>
    </div>

    <!-- DVD-disc size -->
    <div class="form-group no_display"  id="dvd_specs">
        <label for="input_size">Size (in MB)</label>
        <input type="number" class="required form-control" step="0.01" id="input_size" name="size" />
    </div>

    <!-- Book weight -->
    <div class="form-group weightBlock no_display" id="books_specs">
        <label for="input_weight">Weight (in Kg)</label>
        <input type="number" class="required form-control" step="0.01" id="input_weight" name="weight" />
    </div>

    <!-- Dimensions (HxWxL) for ​Furniture -->
    <div class="form-group no_display no_vid" id="furniture_specs">
        <label for="input_dimensions_h">Dimensions (HxWxL)</label>
        <div class="form-group">
            <div>
            <label>Height </label>
            <input type="number" step="0.01" class="required form-control" id="input_dimensions_h" name="dimensions_h" />
            </div>
            <div>
            <label>Width </label>
            <input type="number" step="0.01" class="required form-control" id="input_dimensions_w" name="dimensions_w" />
            </div>
            <div>
            <label>Lenght </label>
            <input type="number" step="0.01" class="required form-control" id="input_dimensions_l" name="dimensions_l" />
            </div>
        </div>
    </div>
    <hr/>
</form>
