<!DOCTYPE html>
<html>
<head>
    <title>Test task</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>

<header class="container">

</header>

<!-- Begin page content -->
<main role="main" class="container">

           <?php include 'application/views/'.$content_view; ?>

</main>

<footer>
    <p class="text-center">Scandiweb  test assignment</p>
</footer>

<script src="js/jquery.min.js"></script>
<script src="js/jquery_validate.js"></script>
<script src="js/ruleValidate.js"></script>
<script src="js/switcherType.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>


