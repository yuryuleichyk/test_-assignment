<?php


    echo'<form method="post" action="/del">
            <div class="row">
                <div class="col-3">
                    <h2>Products list</h2>
            
                </div>
                <div class="col-3 ml-auto">
                    <button type="submit" class="btn btn-danger float-md-right mt-2 mr-2 btn-sm">Mass delete</button>
                    <a href="/create" class="btn btn-success active btn-sm float-md-right mt-2 mr-2" role="button" aria-pressed="true">Add</a>
    
                </div>
            </div>
                <hr/>
                <div class="row">';

    while ($row = $data->fetch(PDO::FETCH_ASSOC))
    {
        if(isset($row['sku'])) {
            echo'<div class="col-md-2 product_grid_item mt-3 mr-3 p-3">';
            echo'<input class="product_grid_item_checkbox float-md-left mt-2" type="checkbox" name="delete_products[]" value='.$row['id'].'>';

            switch ($row['name_type']) {
                case 'dvd':
                    echo $row['sku'] . '<br>';
                    echo $row['name'] . '<br>';
                    echo $row['price'] . ' $<br>';
                    echo 'Size: ' . $row['size'] . 'MB<br>';
                    break;

                case 'book':
                    echo $row['sku'] . '<br>';
                    echo $row['name'] . '<br>';
                    echo $row['price'] . ' $<br>';
                    echo 'Weight: ' . $row['weight'] . 'KG<br>';
                    break;

                case 'furniture':
                    echo $row['sku'] . '<br>';
                    echo $row['name'] . '<br>';
                    echo $row['price'] . ' $<br>';
                    echo 'Dimensions: ' . $row['height'] . 'x' . $row['width'] . 'x' . $row['length'] . '<br>';
                    break;
            }
        }

        echo'</div>';


    }
    echo' </div>
             <hr/>
        </form>';


