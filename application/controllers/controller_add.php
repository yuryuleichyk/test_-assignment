<?php

class Controller_Add extends Controller
{
    public function __construct()
    {
        $productRepository = new ProductRepository();
        $productRepository->createProduct($_POST);
        $this->view = new View();
    }

    public function action_index()
    {
        $this->view->generate('create_view.php', 'template_view.php');
    }

}
