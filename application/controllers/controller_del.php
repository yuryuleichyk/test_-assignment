<?php

class Controller_Del extends Controller
{
    function __construct()
    {
        if(isset($_POST['delete_products']) && is_array($_POST['delete_products']))
        {
            $productRepository = new ProductRepository();
            $productRepository->bulkDelete($_POST['delete_products']);
        }
        $this->model = new Model_Del();
        $this->view = new View();
    }



	function action_index()
	{
        $data = $this->model->get_data();
		$this->view->generate('main_view.php', 'template_view.php', $data);
	}

}
