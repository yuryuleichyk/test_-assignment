<?php

require_once 'AbstractProduct.php';

class DvdProduct extends AbstractProduct
{
    public const PRODUCT_TYPE = 'dvd';
    public const PRODUCT_TYPE_NUMBER = 1;
    /**
     * @var mixed
     */
    protected $size;

    public function __construct(array $productData)
    {
        parent::__construct($productData);

        $this->size = $productData['size'];
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    public function save($pdoConnector)
    {
        $this->filterData();
        $query = $pdoConnector->prepare('CALL SetDvdProduct(:sku, :name, :price, :size, :type)');
        $query->execute(['sku' => $this->sku, 'name' => $this->name,'price' => $this->price,'size' => $this->size,
                            'type' => self::PRODUCT_TYPE_NUMBER]);
    }

    public function delete($pdoConnector)
    {
        // TODO: Implement delete() method.
    }

    public function filterData()
    {
        // TODO: Implement filterData() method.
    }
}