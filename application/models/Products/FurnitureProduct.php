<?php

require_once 'AbstractProduct.php';

class FurnitureProduct extends AbstractProduct
{
    public const PRODUCT_TYPE = 'furniture';
    public const PRODUCT_TYPE_NUMBER = 3;

    /**
     * @var mixed
     */
    protected $height;
    protected $width;
    protected $length;

    public function __construct(array $productData)
    {
        parent::__construct($productData);
        $this->height = floatval($productData['dimensions_h']);
        $this->width = floatval($productData['dimensions_w']);
        $this->length = floatval($productData['dimensions_l']);
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    public function save($pdoConnector)
    {
        $this->filterData();
        $query = $pdoConnector->prepare('CALL SetFurnitureProduct( :sku, :name, :price, :height, :width, :length, :type )');
        $query->execute(['sku' => $this->sku, 'name' => $this->name, 'price' => $this->price, 'height' => $this->height,
                            'width' => $this->width, 'length' => $this->length, 'type' => self::PRODUCT_TYPE_NUMBER]);
   }

    public function delete($pdoConnector)
    {
        // TODO: Implement delete() method.
    }

    public function filterData()
    {
        // TODO: Implement filterData() method.
    }

}