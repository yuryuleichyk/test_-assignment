<?php
require_once 'application/core/Interfaces/ProductInterface.php';

abstract class AbstractProduct implements ProductInterface
{
    public $sku;
    public $name;
    public $price;

    public function __construct(array $productData)
    {
        $this->sku = strval($productData['sku']);
        $this->name = strval($productData['name']);
        $this->price = strval($productData['price']);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getSKU(): string
    {
        return $this->sku;
    }

    public function filterData()
    {

    }
}