<?php

require_once 'AbstractProduct.php';

class BookProduct extends AbstractProduct
{
    public const PRODUCT_TYPE = 'book';
    public const PRODUCT_TYPE_NUMBER = 2;

    protected $weight;

    public function __construct(array $productData)
    {
        parent::__construct($productData);

        $this->weight = floatval($productData['weight']);
    }

    public function save($pdoConnector)
    {
        $this->filterData();
        $query = $pdoConnector->prepare('CALL SetBookProduct(:sku, :name, price, :weight, :type)');
        $query->execute(['sku' => $this->sku, 'name' => $this->name,'price' => $this->price,'weight' => $this->weight,
                            'type' => self::PRODUCT_TYPE_NUMBER]);
    }

    public function delete($pdoConnector)
    {
        // TODO: Implement delete() method.
    }

    public function getWeigh()
    {
        return $this->weight;
    }

    public function filterData()
    {
        // TODO: Implement filterData() method.
    }

}