<?php

class ProductRepository
{

    protected $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;dbname=id16603665_test', 'id16603665_root', 'TutYbf77k@sG6FRC');
    }

    public function createProduct(array $productData)
    {
        $product = $this->getProduct($productData);
        $product->save($this->db);
    }

    public function getProductById(int $id): ProductInterface
    {
        $sql = "SELECT * FROM products WHERE id = $id";
       // $productData = // data

        return $this->getProduct($productData);
	}

    public function deleteProductById(int $id)
    {

        $query = $this->db->prepare('CALL DelProduct(:id)');
        $query->execute(['id' => $id]);
//        $product = $this->getProductById($id);
//        $product->delete($this->db);
    }

    public function bulkDelete($ids = [])
    {
        foreach ($ids as $id) {
            $this->deleteProductById($id);
        }
    }


    protected function getProduct(array $productData): ProductInterface
    {
        switch (true) {
            case $productData['type'] == BookProduct::PRODUCT_TYPE:
                return new BookProduct($productData);
                break;

            case $productData['type'] == DvdProduct::PRODUCT_TYPE:
                return new DvdProduct($productData);
                break;

            case $productData['type'] == FurnitureProduct::PRODUCT_TYPE:
                return new FurnitureProduct($productData);
                break;

            default:
                throw new ProductNotFoundExceptions();
        }
	}
}
