-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 04 2021 г., 10:22
-- Версия сервера: 5.6.47
-- Версия PHP: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tests`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `DelProduct` (IN `id` INT(11))  NO SQL
DELETE FROM `products` WHERE `products`.`id` = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetProducts` ()  BEGIN
			SELECT 
              products.id,
              products.sku,
              products.name,
              products.price,
              products.products_type as p_type,
              (SELECT name_type FROM products_types WHERE products_types.id = p_type) AS name_type,
              (SELECT bookproduct.weight FROM bookproduct WHERE bookproduct.product_id = products.id) AS weight,
              (SELECT size FROM dvdproduct WHERE dvdproduct.product_id = products.id) AS size,
              (SELECT height FROM furnitureproduct WHERE furnitureproduct.product_id = products.id) AS height,
              (SELECT width FROM furnitureproduct WHERE furnitureproduct.product_id = products.id) AS width,
              (SELECT length FROM furnitureproduct WHERE furnitureproduct.product_id = products.id) AS length

            FROM
               products  ORDER BY id DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SetBookProduct` (IN `sku` VARCHAR(50), IN `name` VARCHAR(50), IN `price` FLOAT, IN `weight` FLOAT, IN `type` SMALLINT(1))  BEGIN

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `products_type`) VALUES (NULL, sku, name, price, type);
INSERT INTO `bookproduct` (`id`, `weight`, `product_id`) VALUES (NULL, weight, LAST_INSERT_ID());

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SetDvdProduct` (IN `sku` VARCHAR(50), IN `name` VARCHAR(50), IN `price` FLOAT, IN `size` FLOAT, IN `type` SMALLINT(1))  BEGIN

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `products_type`) VALUES (NULL, sku, name, price, type);
INSERT INTO `dvdproduct` (`id`, `size`, `product_id`) VALUES (NULL, size, LAST_INSERT_ID());

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SetFurnitureProduct` (IN `sku` VARCHAR(50), IN `name` VARCHAR(50), IN `price` FLOAT, IN `height` FLOAT, IN `width` FLOAT, IN `length` FLOAT, IN `type` SMALLINT(1))  BEGIN

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `products_type`) VALUES (NULL, sku, name, price, type);
INSERT INTO `furnitureproduct` (`id`, `height`, `width`, `length`, `product_id`) VALUES (NULL, height, width, length, LAST_INSERT_ID());

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `bookproduct`
--

CREATE TABLE `bookproduct` (
  `id` int(11) NOT NULL,
  `weight` float NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `bookproduct`
--

INSERT INTO `bookproduct` (`id`, `weight`, `product_id`) VALUES
(2, 2, 5),
(3, 2, 7),
(4, 3, 8),
(5, 5, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `dvdproduct`
--

CREATE TABLE `dvdproduct` (
  `id` int(11) NOT NULL,
  `size` float NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dvdproduct`
--

INSERT INTO `dvdproduct` (`id`, `size`, `product_id`) VALUES
(2, 500, 6),
(3, 670, 13),
(4, 652, 14);

-- --------------------------------------------------------

--
-- Структура таблицы `furnitureproduct`
--

CREATE TABLE `furnitureproduct` (
  `id` int(11) NOT NULL,
  `height` float NOT NULL,
  `width` float NOT NULL,
  `length` float NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `furnitureproduct`
--

INSERT INTO `furnitureproduct` (`id`, `height`, `width`, `length`, `product_id`) VALUES
(2, 100, 120, 140, 16);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `products_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `products_type`) VALUES
(5, 'pdo171', 'fdfdf', 55, 2),
(6, 'pdo172', 'fdfdf33', 55, 1),
(7, 'pdo173', 'fdfdf334', 24, 2),
(8, 'pdo174', 'Harry Poter', 22, 2),
(10, 'pdo343', 'book1', 343, 2),
(13, 'pdo465', 'DVD1', 23, 1),
(14, 'pdo669', 'DVD', 44, 1),
(16, 'pdo179', 'sofa', 4545, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `products_types`
--

CREATE TABLE `products_types` (
  `id` int(11) NOT NULL,
  `name_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products_types`
--

INSERT INTO `products_types` (`id`, `name_type`) VALUES
(1, 'dvd'),
(2, 'book'),
(3, 'furniture');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `bookproduct`
--
ALTER TABLE `bookproduct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `dvdproduct`
--
ALTER TABLE `dvdproduct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `furnitureproduct`
--
ALTER TABLE `furnitureproduct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `furnitureproduct_ibfk_1` (`product_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`),
  ADD KEY `products_type` (`products_type`);

--
-- Индексы таблицы `products_types`
--
ALTER TABLE `products_types`
  ADD UNIQUE KEY `id` (`id`) USING BTREE;

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `bookproduct`
--
ALTER TABLE `bookproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `dvdproduct`
--
ALTER TABLE `dvdproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `furnitureproduct`
--
ALTER TABLE `furnitureproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `bookproduct`
--
ALTER TABLE `bookproduct`
  ADD CONSTRAINT `bookproduct_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `dvdproduct`
--
ALTER TABLE `dvdproduct`
  ADD CONSTRAINT `dvdproduct_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `furnitureproduct`
--
ALTER TABLE `furnitureproduct`
  ADD CONSTRAINT `furnitureproduct_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`products_type`) REFERENCES `products_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
